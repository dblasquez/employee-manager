# Employee Manager

## Django Admin Panel
Login: diego  
password: 123456

## Endpoint

**List**
- Method: GET
- Path: /employee/
- Return:
```
[
    {
        "id": 1,
        "name": "name one",
        "email": "nameone@gmail.com",
        "department": "IT"
    },
    {
        "id": 1,
        "name": "name one",
        "email": "nameone@gmail.com",
        "department": "IT"
    }
]
```

**Add**
- Method: POST
- Path: /employee/
- Body request:
```
[
    "employee":
                {
                    "id": 1,
                    "name": "name one",
                    "email": "nameone@gmail.com",
                    "department": "IT"
                }
]
```
- Return: 
```
{
    "success": "Employee 'name' created successfully"
}
```

**Remove**
- Method: DELETE
- Path: /employee/EMPLOYEE_ID


