from rest_framework import serializers
from .models import Employee

class EmployeeSerializer(serializers.ModelSerializer):
	name=serializers.CharField(max_length=150)
	email=serializers.CharField(max_length=255)
	department=serializers.CharField(max_length=100)
		
	def create(self, validated_data):
		return Employee.objects.create(**validated_data)
	
	class Meta:
		model=Employee
		fields='__all__'