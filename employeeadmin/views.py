from django.shortcuts import render

from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import Employee
from .serializers import EmployeeSerializer

class EmployeeView(APIView):

	def get(self, request):
		model=Employee.objects.all();
		serializer=EmployeeSerializer(model, many=True)
		return Response(serializer.data)
	
	def post(self, request):
		model=request.data.get('employee')
		serializer = EmployeeSerializer(data=model)
		if serializer.is_valid(raise_exception=True):
			employee_saved = serializer.save()
		return Response({"success": "Employee '{}' created successfully".format(employee_saved.name)})
		
	def delete(self, request, pk):
		model = get_object_or_404(Employee.objects.all(), pk=pk)
		model.delete()
		return Response({"message": "Employee with id `{}` has been deleted.".format(pk)},status=204)	


