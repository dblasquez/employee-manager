from django.apps import AppConfig


class EmployeeadminConfig(AppConfig):
    name = 'employeeadmin'
